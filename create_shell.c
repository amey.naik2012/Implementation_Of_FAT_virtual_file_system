#include "header.h"
#define LSH_TOK_BUFSIZE 100
#define LSH_TOK_DELIM " \t\r\n\a"

 char *get_dir_from_abs_path_except_dir(char *ptr)
    {
         int i=0;
         char * ret_ptr = malloc(sizeof(char)*strlen(ptr));
          if(ptr[strlen(ptr)-1] == '/' )
          {
      strcpy(ret_ptr,ptr);
      return (ret_ptr + (strlen(ptr)-1));
     }
    for(i=strlen(ptr);i>-1;i--)
     {
        if(ptr[i]=='/')
          break;
    }
     if(i < 0)
     return NULL;

    strcpy(ret_ptr,ptr);
    ret_ptr[i+1]='\0';

    return (ret_ptr);
}

 char *get_dir_from_abs_path(char *ptr)
 {
    int i=0;
   for(i=strlen(ptr);i>-1;i--)
   {
      if(ptr[i]=='/')
        break;
    }

    return (ptr + i+1);
 }




char **tokenize(char *line)
{
    int bufsize = LSH_TOK_BUFSIZE, position = 0;
    char **tokens = malloc(bufsize * sizeof(char*));
    char *token;

    if (!tokens) {
      fprintf(stderr, "lsh: allocation error\n");
       exit(EXIT_FAILURE);
     }

     token = strtok(line, LSH_TOK_DELIM);
     while (token != NULL) {
         tokens[position] = token;
         position++;

        if (position >= bufsize) {
          bufsize += LSH_TOK_BUFSIZE;
          tokens = realloc(tokens, bufsize * sizeof(char*));
        if (!tokens) {
              fprintf(stderr, "lsh: allocation error\n");
              exit(EXIT_FAILURE);
         }
      }

     token = strtok(NULL, LSH_TOK_DELIM);
   }
   tokens[position] = NULL;
  return tokens;
}
void print_tokens(char **token){
  int i = 0;
  while(token[i]!=NULL){
    printf("%d -> %s \n",i,token[i]);
    i++;
  }
}

int get_number_of_tokens(char **token){
  int count = 0;
  while(token[count]!=NULL){
    count++;
  }
  return count;
}

int get_file_inode_from_current_dir(char* ptr_to_block,char *file_name){
  directory_row_field *ptr_to_row;
  ptr_to_row=(directory_row_field *)ptr_to_block;
  int count=0,res;
  while((count < MAXIMUM_ROW_FIELDS_IN_DIR_BLOCK)){
           if(ptr_to_row -> access_flag == '1')
            {
               if(strcmp(ptr_to_row->name_of_file,file_name)==0){
                return (ptr_to_row -> inode_id);
               }
            }       
   
           count++;
           ptr_to_row ++;

        }     
         
         res = is_next_block_preset(ptr_to_block);
         if(res==0){
           return -1;
         }
         else{
            ptr_to_block =(char*) (get_next_block_address (ptr_to_block));
            return get_file_inode_from_current_dir(ptr_to_block,file_name);
         }
}
void create_$_promt(hdd *hdd_struct){
  system("clear");
  int temp;
  char *input = (char*)malloc(100);
  char *file_name = (char*)malloc(120);
  char *current_path = (char*)malloc(100);
  while(1){

  char **tokens;

  printf("roshniameyshubham@debian:%s$ ",current_path);
  fflush(stdin);
  fgets(input,99,stdin);
  tokens = tokenize(input);
  if(strcmp(tokens[0],"ls")==0)
   {

print_content_of_inode(hdd_struct -> ptr_to_dir_inode);

}
  else if(strcmp(tokens[0],"makedir")==0)
   {
   if(get_number_of_tokens(tokens)!=2){
     printf("bash : Too few agruments\n");
     continue;
   }

       char* temp_ptr_to_cur_inode;
       temp_ptr_to_cur_inode = hdd_struct->ptr_to_dir_inode;
       char *temp_path;
       temp_path = get_dir_from_abs_path_except_dir(tokens[1]);
       if(temp_path != NULL)
       {
          if(0==strlen(temp_path))
           {
          printf("bash : makedir no path found %s\n",tokens[1]);
           free(temp_path);
           continue;
           }
         else
          {
           char *temp_path1 = (char*)malloc(100);

    strcpy(temp_path1,(char*)change_promt_for_cd_command(hdd_struct,current_path,temp_path));
          if(0 != strcmp(current_path,temp_path1))
             {
             make_dir(hdd_struct,get_dir_from_abs_path(tokens[1]));
             }
          }
          hdd_struct->ptr_to_dir_inode = temp_ptr_to_cur_inode;
       }
       else
       make_dir(hdd_struct,tokens[1]);
   }
  else if(strcmp(tokens[0],"clear")==0)
   {
     system("clear");
   }
  else if(strcmp(tokens[0],"cd")==0){
    
   if(get_number_of_tokens(tokens)!=2){
     printf("bash : Too few agruments\n");
     continue;
   }

    char *temp_path = (char*)malloc(100);
   
    strcpy(temp_path,(char*)change_promt_for_cd_command(hdd_struct,current_path,tokens[1]));
    strcpy(current_path,temp_path);

  }
  else if(strcmp(tokens[0],"rmdir")==0){
   if(get_number_of_tokens(tokens)!=2){
     printf("bash : Too few agruments\n");
     continue;
   }
    remove_dir(hdd_struct,tokens[1]);
  }
  
  else if(strcmp(tokens[0],"create")==0){
   if(get_number_of_tokens(tokens)!=2){
     printf("bash : Too few agruments\n");
     continue;
   }

       char* temp_ptr_to_cur_inode;
       temp_ptr_to_cur_inode = hdd_struct->ptr_to_dir_inode;
       char *temp_path;
       temp_path = get_dir_from_abs_path_except_dir(tokens[1]);
       if(temp_path != NULL)
       {
          if(0==strlen(temp_path))
           {
            printf("bash : create no path found %s\n",tokens[1]);
            free(temp_path);
            continue;
           }

         else
          {

            char *temp_path1 = (char*)malloc(100);
          strcpy(temp_path1,(char*)change_promt_for_cd_command(hdd_struct,current_path,temp_path));
          if(0 != strcmp(current_path,temp_path1))
             {

             create_new_file(hdd_struct,get_dir_from_abs_path(tokens[1]));
             }
          }
          hdd_struct->ptr_to_dir_inode = temp_ptr_to_cur_inode;
       }
       else
       create_new_file(hdd_struct,tokens[1]);
   }



  else if(strcmp(tokens[0],"cat")==0)
     {
   if(get_number_of_tokens(tokens)!=2){
    printf("bash : Too few agruments\n");
     continue;
   }



      char* temp_ptr_to_cur_inode;
       temp_ptr_to_cur_inode = hdd_struct->ptr_to_dir_inode;
       char *temp_path;
       temp_path = get_dir_from_abs_path_except_dir(tokens[1]);
       if(temp_path != NULL)
       {
          if(0==strlen(temp_path))
           {
           printf("bash : cat no path found %s\n",tokens[1]);
           free(temp_path);
           continue;
           }
           else
           {
           char *temp_path1 = (char*)malloc(100);
           strcpy(temp_path1,(char*)change_promt_for_cd_command(hdd_struct,current_path,temp_path));
          if(0 != strcmp(current_path,temp_path1))
             {
           inode_struct *ptr_cur_file_inode_struct;
          int size_of_file;
          int  inode_id = get_file_inode_from_current_dir((char*)(((inode_struct*)( hdd_struct-> ptr_to_dir_inode))->file_start_address),get_dir_from_abs_path(tokens[1]));

               if( inode_id == -1){
           create_new_file(hdd_struct,get_dir_from_abs_path(tokens[1]));
                          }

             inode_id = get_file_inode_from_current_dir((char*)(((inode_struct*)( hdd_struct-> ptr_to_dir_inode))->file_start_address),get_dir_from_abs_path(tokens[1]));
              ptr_cur_file_inode_struct = (inode_struct*)get_inode_base_address(hdd_struct,inode_id-1);
               while((strcmp(fgets(input,80,stdin),"EOF"))!=0)
               {

                 if((strcmp(input,"EOF\n"))==0)
                 break;
               // printf("+++++++++");
               write_to_exixt_file(hdd_struct,(char*)(ptr_cur_file_inode_struct)->file_start_address,input,ptr_cur_file_inode_struct->file_size);
               ptr_cur_file_inode_struct -> file_size += strlen(input);
               }

              }
          hdd_struct->ptr_to_dir_inode = temp_ptr_to_cur_inode;
           }
   }
else
    { 
           inode_struct *ptr_cur_file_inode_struct;
          int size_of_file;
        int  inode_id = get_file_inode_from_current_dir((char*)(((inode_struct*)( hdd_struct-> ptr_to_dir_inode))->file_start_address),tokens[1]);

               if( inode_id == -1){
           create_new_file(hdd_struct,tokens[1]);
                          }
             inode_id = get_file_inode_from_current_dir((char*)(((inode_struct*)( hdd_struct-> ptr_to_dir_inode))->file_start_address),tokens[1]);
              ptr_cur_file_inode_struct = (inode_struct*)get_inode_base_address(hdd_struct,inode_id-1);

       while((strcmp(fgets(input,80,stdin),"EOF"))!=0)
       {
        if((strcmp(input,"EOF\n"))==0)
        break;
        write_to_exixt_file(hdd_struct,(char*)(ptr_cur_file_inode_struct)->file_start_address,input,ptr_cur_file_inode_struct->file_size);
        ptr_cur_file_inode_struct -> file_size += strlen(input);
        }
         
       }
     }

  else if(strcmp(tokens[0],"less")==0){

   if(get_number_of_tokens(tokens)!=2){
     printf("bash : Too few agruments\n");
     continue;
   }
       char* temp_ptr_to_cur_inode;
       temp_ptr_to_cur_inode = hdd_struct->ptr_to_dir_inode;
       char *temp_path;
       temp_path = get_dir_from_abs_path_except_dir(tokens[1]);
       if(temp_path != NULL)
       {
          if(0==strlen(temp_path))
           {
          printf("bash : less no path found %s\n",tokens[1]);
           free(temp_path);
           continue;
           }
         else
          {
           char *temp_path1 = (char*)malloc(100);

    strcpy(temp_path1,(char*)change_promt_for_cd_command(hdd_struct,current_path,temp_path));
          if(0 != strcmp(current_path,temp_path1))
             {
        inode_struct *ptr_cur_file_inode_struct;
        int size_of_file;
        int inode_id = get_file_inode_from_current_dir((char*)(((inode_struct*)(hdd_struct -> ptr_to_dir_inode))->file_start_address) ,get_dir_from_abs_path(tokens[1]));
        if( inode_id == -1){
           printf("No such file or directory..");
         }
        ptr_cur_file_inode_struct =(inode_struct *) get_inode_base_address(hdd_struct,inode_id - 1);
        size_of_file = ptr_cur_file_inode_struct -> file_size;
             read_from_file(hdd_struct,((inode_struct*)ptr_cur_file_inode_struct)->file_start_address,size_of_file);
             printf("\n");
             }
          }
          hdd_struct->ptr_to_dir_inode = temp_ptr_to_cur_inode;
       }
       else
       {
        inode_struct *ptr_cur_file_inode_struct;
        int size_of_file;
        int inode_id = get_file_inode_from_current_dir((char*)(((inode_struct*)(hdd_struct -> ptr_to_dir_inode))->file_start_address) ,tokens[1]);
      if( inode_id == -1){
           printf("No such file or directory..");
         }
        ptr_cur_file_inode_struct =(inode_struct *) get_inode_base_address(hdd_struct,inode_id - 1);
       size_of_file = ptr_cur_file_inode_struct -> file_size;
       read_from_file(hdd_struct,((inode_struct*)ptr_cur_file_inode_struct)->file_start_address,size_of_file);
      }
}

  else if(strcmp(tokens[0],"help")==0){
                
   if(get_number_of_tokens(tokens)!=1){
        printf("bash : Too few agruments\n");
        continue;
      }

printf("_________________________________________________________________________________________________________________________\n");
printf("operation                                             |       command                                                   |\n ");
printf("------------------------------------------------------------------------------------------------------------------------|\n");
printf("change directory                                      |  cd [file_name]                                                 |\n");
printf("----------------------------------------------------------------------------------------------------------------------- |\n ");
printf("list directory content                                | ls [directory_name]                                             |\n");
printf("------------------------------------------------------------------------------------------------------------------------|\n");
printf("make directory                                        | makedir [directory_name]                                        |\n");
printf("------------------------------------------------------------------------------------------------------------------------|\n");
printf("create a file                                         | create [file_name]                                              |\n");
printf("------------------------------------------------------------------------------------------------------------------------|\n");
printf("insert in file                                        | cat [file_name]                                                 |\n");
printf("------------------------------------------------------------------------------------------------------------------------|\n");
printf("display file content                                  | less [file_name]                                                |\n");
printf("------------------------------------------------------------------------------------------------------------------------|\n");
printf("remove directory                                      | rmdir [file_name]                                               |\n");
printf("------------------------------------------------------------------------------------------------------------------------|\n "); 
  }
  else if(strcmp(tokens[0],"mv") == 0){

   if(get_number_of_tokens(tokens)!=3){
     printf("bash : Too few agruments\n");
     continue;
   }

       // inode_struct *ptr_cur_file_inode_struct1,*ptr_cur_file_inode_struct2;
       char* temp_ptr_to_cur_inode;
       temp_ptr_to_cur_inode = hdd_struct->ptr_to_dir_inode;
       char *temp_path,*temp_path_new;
       temp_path= get_dir_from_abs_path_except_dir(tokens[1]);

       temp_path_new = get_dir_from_abs_path_except_dir(tokens[2]);
        if(temp_path != NULL || temp_path_new != NULL)
       {
          if((0==strlen(temp_path)) && (0==strlen(temp_path_new)) )
           {
            printf("bash : rename no path found %s\n",tokens[1]);
            free(temp_path);
            free(temp_path_new);
            continue;
           }

         else
          {

            char *temp_path1 = (char*)malloc(100);
            char *temp_path2 = (char*)malloc(100);
      

          strcpy(temp_path2,(char*)change_promt_for_cd_command(hdd_struct,current_path,temp_path_new));
          strcpy(temp_path1,(char*)change_promt_for_cd_command(hdd_struct,current_path,temp_path));
          if(0 != strcmp(current_path,temp_path1))
             {
              
              if(0 != strcmp(current_path,temp_path2))
               {
               printf("++++++++++");
              inode_struct *ptr_cur_file_inode_struct1,*ptr_cur_file_inode_struct2;
             rename_file(((inode_struct*)(hdd_struct->ptr_to_dir_inode))->file_start_address,get_dir_from_abs_path(tokens[1]),get_dir_from_abs_path(tokens[2]));
              }
           }

          }
          hdd_struct->ptr_to_dir_inode = temp_ptr_to_cur_inode;
       }
       else
             rename_file(((inode_struct*)(hdd_struct->ptr_to_dir_inode))->file_start_address,tokens[1],tokens[2]);
   }

  

  else if(strcmp(tokens[0],"cp")== 0){
      if(get_number_of_tokens (tokens)!=3){

     printf("bash : Too few agruments\n");
     continue;
   }
       int check;
        inode_struct *ptr_cur_file_inode_struct1,*ptr_cur_file_inode_struct2;
        int size_of_file1,size_of_file2;
     int inode_id1 = get_file_inode_from_current_dir((char*)(((inode_struct*)(hdd_struct -> ptr_to_dir_inode))->file_start_address) ,tokens[1]);
     int inode_id2 = get_file_inode_from_current_dir((char*)(((inode_struct*)(hdd_struct -> ptr_to_dir_inode))->file_start_address) ,tokens[2]);
        ptr_cur_file_inode_struct1 =(inode_struct *) get_inode_base_address(hdd_struct,inode_id1 - 1);
        ptr_cur_file_inode_struct2 =(inode_struct *) get_inode_base_address(hdd_struct,inode_id2 - 1);
       size_of_file1 = ptr_cur_file_inode_struct1 -> file_size;
       printf("%d",((inode_struct*)ptr_cur_file_inode_struct1)->file_start_address);
       printf("%d",((inode_struct*)ptr_cur_file_inode_struct2)->file_start_address);
       size_of_file2 = ptr_cur_file_inode_struct2 -> file_size;

       check=copy_from_file(hdd_struct,((inode_struct*)ptr_cur_file_inode_struct1)->file_start_address,size_of_file1,((inode_struct*)ptr_cur_file_inode_struct2)->file_start_address);
       printf("\n");
       printf("%d",check);
  }


     
     else{
printf("bash: %s: command not found\n",tokens[0]);

  }
  }
}
