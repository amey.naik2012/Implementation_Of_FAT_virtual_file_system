#include "header.h"
int get_inode_id(char* inode_base_address){
  return (int)(((inode_struct*)inode_base_address)-> inode_id);
}
int make_dir(hdd *hdd_struct,char *dir_name){


  directory_row_field *temp_record;
  inode_struct *new_dir_inode;
  int index_of_inode_in_array;
  index_of_inode_in_array = get_inode(hdd_struct);
  new_dir_inode = (inode_struct *) get_inode_base_address(hdd_struct,index_of_inode_in_array);
  new_dir_inode = (inode_struct *) update_inode(hdd_struct,new_dir_inode,DIRECTORY_FLAG);
  
	format_directory_block((char*)new_dir_inode -> file_start_address);
  
	temp_record = (directory_row_field*)create_dir_row((int)new_dir_inode -> inode_id ,".");
	insert_record_in_dir_block(hdd_struct,new_dir_inode -> file_start_address,temp_record );
  
  //printf("inode id of parent : %d",((inode_struct*)hdd_struct -> ptr_to_dir_inode)-> inode_id);
	temp_record =(directory_row_field*) create_dir_row((((inode_struct*)hdd_struct -> ptr_to_dir_inode)-> inode_id),"..");
	insert_record_in_dir_block(hdd_struct,new_dir_inode -> file_start_address,temp_record );

	temp_record = (directory_row_field*)create_dir_row(get_inode_id((char*)new_dir_inode ) , dir_name);
	insert_record_in_dir_block(hdd_struct,((inode_struct*)(hdd_struct -> ptr_to_dir_inode))-> file_start_address ,temp_record );
 
	//print_content_of_inode(new_dir_inode);
 //printf("_____________________________________________________________________________________________\n"); 
 //print_content_of_inode((hdd_struct -> ptr_to_dir_inode));
  
}
