#include "header.h"

char* alloc(hdd *hdd_struct,int number_of_requested_block)
{
  int alloc_cnt=0;
  char  *next_block_address;

  update_metadata_struct(hdd_struct);
  hdd_struct -> ptr_to_hdd = hdd_struct -> ptr_to_metadata -> head_of_free_block_list ;

  char *temp_address = hdd_struct -> ptr_to_hdd;
 
 if( hdd_struct -> ptr_to_metadata -> number_of_free_blocks > number_of_requested_block ){
   hdd_struct -> ptr_to_metadata -> number_of_free_blocks -= number_of_requested_block ;
    while(number_of_requested_block > 1){
      next_block_address = (char *)*(int *)(hdd_struct -> ptr_to_hdd + LOCATION_OF_NEXT_AND_PREV + 4);
      hdd_struct -> ptr_to_hdd = next_block_address ; 
      number_of_requested_block--;
     }

    next_block_address = (char *)*(int*)(hdd_struct -> ptr_to_hdd + LOCATION_OF_NEXT_AND_PREV + 4);
    *(int*)(hdd_struct -> ptr_to_hdd + LOCATION_OF_NEXT_AND_PREV + 4) = -1;
    *(int*)(next_block_address + LOCATION_OF_NEXT_AND_PREV) = -1;
   hdd_struct -> ptr_to_metadata -> head_of_free_block_list = (char*)next_block_address;
    
  update_metadata(hdd_struct);
    return temp_address; 
  }
  else
  return NULL;

}
