#include "header.h"

int get_inode(hdd *hdd_struct){
    char *ptr_to_inode_array =  hdd_struct -> ptr_to_metadata -> address_of_inode_list;
	int index;
	for(index=0;index < NUMBER_OF_INODES ;index++){
		if(ptr_to_inode_array[index]=='0'){
                    hdd_struct -> ptr_to_metadata -> number_of_inodes -= 1;
                    ptr_to_inode_array[index]='1';
                    update_metadata(hdd_struct);
	            return (index);
                }
	}
	return -1;
}

char* get_inode_base_address(hdd *hdd_struct,int index){
	return  (hdd_struct -> hdd_base + get_block(1))+ (index * SIZE_OF_INODE);
}

inode_struct*  update_inode(hdd *hdd_struct,inode_struct *ptr_to_inode_struct,int file_type_flag){
  if(file_type_flag == 0){
	  ptr_to_inode_struct -> file_type = 0 ;
  }
  else
  {
    ptr_to_inode_struct -> file_type = 1 ;
  }
	//ptr_to_inode_struct -> inode_id  = 1 ;
	 ptr_to_inode_struct -> file_size = 0 ;
	 ptr_to_inode_struct -> file_start_address = alloc(hdd_struct,1);
   return ptr_to_inode_struct;
}

void print_perticular_inode(inode_struct *inode_ptr){

	printf("inode id : %d\n",inode_ptr->inode_id );
	printf("inode filetype : %d\n",inode_ptr->file_type );
	printf("inode filesize : %d\n",inode_ptr->file_size );
	printf("inode file start addrs : %d\n",inode_ptr->file_start_address );
}

directory_row_field * create_dir_row(int inode_no,char *file_name)
{
	directory_row_field *ptr_to_record = malloc(sizeof(directory_row_field));  
	ptr_to_record->inode_id = inode_no;
	ptr_to_record -> access_flag = '1';
	strcpy(ptr_to_record->name_of_file,file_name);
	return ptr_to_record;
}

void print_directory_block_content_testing(char *ptr_to_block){
	directory_row_field *ptr_to_record = (directory_row_field*)ptr_to_block;
  int row_offset = 0;
  while(row_offset < MAXIMUM_ROW_FIELDS_IN_DIR_BLOCK){
     if(ptr_to_record -> access_flag == '1')
	    printf("access flag : %c \t inode_id : %d \t filename : %s\n",ptr_to_record -> access_flag ,ptr_to_record -> inode_id, ptr_to_record -> name_of_file);
    ptr_to_record++;
    row_offset++;
  }
  if( is_next_block_preset(ptr_to_block)){
    print_directory_block_content_testing(ptr_to_block);
  }
}
void print_directory_block_content(char *ptr_to_block){
	directory_row_field *ptr_to_record = (directory_row_field*)ptr_to_block;
  int row_offset = 0;
  while(row_offset < MAXIMUM_ROW_FIELDS_IN_DIR_BLOCK){
     if(ptr_to_record -> access_flag == '1')
	    printf("%s\n", ptr_to_record -> name_of_file);
	    //printf("access flag : %c \t inode_id : %d \t filename : %s\n",ptr_to_record -> access_flag ,ptr_to_record -> inode_id, ptr_to_record -> name_of_file);
    ptr_to_record++;
    row_offset++;
  }
  if( is_next_block_preset(ptr_to_block)){
    print_directory_block_content(ptr_to_block);
   
  }
}

void print_content_of_inode(inode_struct *inode_ptr){
	char *ptr_to_record = (char*)(inode_ptr -> file_start_address);
	print_directory_block_content(ptr_to_record);
}

void initialize_root_directory(hdd *hdd_struct){
	int index_of_inode_in_array,inode_id;
	directory_row_field* temp_record;
	inode_struct *root_inode;
	index_of_inode_in_array  =  get_inode(hdd_struct);
	if(index_of_inode_in_array == -1){
		printf("ERROR ___ NO MEMORY AVAILABLE \n");
		exit(0);
	}
	root_inode =(inode_struct *)get_inode_base_address(hdd_struct,index_of_inode_in_array);
  hdd_struct -> ptr_to_dir_inode = (char*)root_inode;
	update_inode(hdd_struct,root_inode,0);
	format_directory_block((char*)root_inode->file_start_address);
	temp_record = create_dir_row(root_inode -> inode_id ,".");
	insert_record_in_dir_block(hdd_struct,root_inode -> file_start_address,temp_record );
/*
 * this is testing part::
	//print_content_of_inode(root_inode);
  //print_metadata(hdd_struct);
//make_dir(hdd_struct,"hello");

	print_content_of_inode(root_inode);
 //      printf("\n deleting form dir \n");
     inode_id = delete_record_from_directory(root_inode->file_start_address,"hello");
  //   printf("\nafter delete rec = %d\n",inode_id);
     if( -1 == inode_id ){
         printf(" rm: cannot remove ‘m.c’: No such file or directory");
       }


	print_content_of_inode(root_inode);
  print_perticular_inode(root_inode);
  printf("________________________________________________________________________________________________________");
 // delete_i_inode_from_inode_struct(inode_id,hdd_struct );

 //       printf("\n After deletion : \n"); 
	//print_content_of_inode(root_inode);
	//test_format_dir_block(root_inode->file_start_address);
    //    printf("after  del root : ____________________________\n");
    //    print_metadata(hdd_struct);
 *********************testing part over***********/
}
