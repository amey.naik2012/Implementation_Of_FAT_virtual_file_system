#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


#define LOCATION_OF_NEXT_AND_PREV  4088
#define FREE_BLOCK_COUNT  127452
#define NUMBER_OF_DATA_BLOCK 127452
#define NUMBER_OF_INODES 127452
#define SIZE_OF_BLOCK 4096
#define SIZE_OF_INODE 16
#define SIZE_OF_DIRECTORY_STURCTURE 120
#define MAXIMUM_ROW_FIELDS_IN_DIR_BLOCK 34
#define DIRECTORY_FLAG 0
#define FILE_FLAG 1

typedef struct directory_row_field
{
  char access_flag;
  int inode_id;
  char name_of_file[112];
}directory_row_field;
typedef struct inode_struct
{
int inode_id;
int file_type;
int file_size;
int file_start_address;
}inode_struct;


typedef struct meta_data
{
  int number_of_inodes;
  char *head_of_free_block_list;
  int number_of_free_blocks;
  char *address_of_inode_list;
}meta_data_struct;
typedef struct hard_disk
{
  char *ptr_to_dir_inode;
  char *ptr_to_hdd;
  char *hdd_base;
  meta_data_struct *ptr_to_metadata;
}hdd;

