#include"header.h"

void free_block(hdd *hdd_struct,char *address_of_new_free_block)
{
  int temp,count=1,nextblock,temp1;
  temp1=(int)address_of_new_free_block;

    while((*(int*)(temp1+LOCATION_OF_NEXT_AND_PREV+4)) != -1)
        {
          nextblock=*(int*)(temp1+LOCATION_OF_NEXT_AND_PREV+4);
          temp1=nextblock;
          count++;
        }
 
  hdd_struct-> ptr_to_hdd=hdd_struct->ptr_to_metadata->head_of_free_block_list;
  temp = (int)hdd_struct->ptr_to_metadata->head_of_free_block_list;
  *(int*)(hdd_struct->ptr_to_hdd+LOCATION_OF_NEXT_AND_PREV)= temp1;
  *(int*)(temp1+LOCATION_OF_NEXT_AND_PREV+4)=temp;
  hdd_struct->ptr_to_metadata->head_of_free_block_list = (char*)(address_of_new_free_block);
  hdd_struct->ptr_to_metadata->number_of_free_blocks += count;
  update_metadata(hdd_struct);

}
