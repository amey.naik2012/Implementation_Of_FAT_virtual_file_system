main1:
	gcc main.c -c
free_block_mechanism:
	gcc free_block_mechanism.c -c
initialize_metadata:
	gcc initialize_metadata.c -c
initialize_inode_block1:
	gcc initialize_inode_block.c -c
allocate_n_block1:
	gcc allocate_n_blocks.c -c
free_n_blocks:
	gcc free_n_blocks.c -c
initialize_root_inode:
	gcc initialize_root_inode.c -c
free_inode_from_array_mechanism:
	gcc free_inode_from_array_mechanism.c -c
format_directory_block:
	gcc format_directory_block.c -c
make_dir:
	gcc make_dir.c -c
remove_dir:
	gcc remove_dir.c -c
write_in_to_a_file:
	gcc write_in_to_a_file.c -c
change_dir:
	gcc change_dir.c -c
create_my_shell:
	gcc create_shell.c -c

change_promt_dir_pwd:
	gcc change_promt.c -c

exe: change_promt_dir_pwd change_dir create_my_shell write_in_to_a_file remove_dir make_dir free_inode_from_array_mechanism format_directory_block initialize_root_inode free_block_mechanism initialize_metadata  initialize_inode_block1 allocate_n_block1 free_n_blocks main1
	gcc change_promt.o change_dir.o create_shell.o write_in_to_a_file.o remove_dir.o make_dir.o free_inode_from_array_mechanism.o format_directory_block.o initialize_root_inode.o initialize_metadata.o free_block_mechanism.o initialize_inode_block.o allocate_n_blocks.o free_n_blocks.o main.o -o exe
clean:
	rm exe *.o

bup:
	cp * ./backup/


